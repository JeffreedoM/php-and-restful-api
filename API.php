<?php

/**
 * Tells the browser to allow code from any origin to access
 */
header("Access-Control-Allow-Origin: *");


/**
 * Tells browsers whether to expose the response to the frontend JavaScript code
 * when the request's credentials mode (Request.credentials) is include
 */
header("Access-Control-Allow-Credentials: true");



/**
 * Specifies one or more methods allowed when accessing a resource in response to a preflight request
 */
header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");

/**
 * Used in response to a preflight request which includes the Access-Control-Request-Headers to
 * indicate which HTTP headers can be used during the actual request
 */
header("Access-Control-Allow-Headers: Content-Type");

require_once 'MysqliDB.php';

class API
{

    public function __construct()
    {
        $this->db = new MysqliDB('localhost', 'root', '', 'employee');
    }

    public function httpGet($payload = array())
    {
        // execute query
        if (isset($payload['id']) || isset($_GET['id'])) {
            if (isset($payload['id'])) {
                $query = $this->db->where('id', $payload['id']);
            } else {
                $query = $this->db->where('id', $_GET['id']);
            }
            $query = $this->db->getOne('information');
        } else {
            $query = $this->db->get('information');
        }

        /**
         * HTTP GET Request
         *
         * @param $payload
         */

        //check if query is success or fail
        if ($query) {
            return json_encode(array(
                'method' => 'GET',
                'status' => 'success',
                'data' => $query,
            ));
        } else {
            return json_encode(array(
                'method' => 'GET',
                'status' => 'fail',
                'data' => [],
                'message' => 'Failed to Fetch'
            ));
        }
    }

    /**
     * HTTP POST Request
     *
     * @param $payload
     */
    public function httpPost($payload)
    {
        //Execute Query
        $query = $this->db->insert('information', $payload);

        // getting last inserted id
        $this->db->orderBy("id", "Desc");
        $results = $this->db->getOne('information');
        $lastinsertId = $results['id'];
        $payload['id'] = $lastinsertId;

        //check if query is success or fail
        if ($query) {
            return json_encode(array(
                'method' => 'POST',
                'status' => 'success',
                'data' => $payload,
            ));
        } else {
            return json_encode(array(
                'method' => 'POST',
                'status' => 'fail',
                'data' => [],
                'message' => 'Failed to Insert'
            ));
        }
    }

    /**
     * HTTP PUT Request
     *
     * @param $id
     * @param $payload
     */
    public function httpPut($id, $payload)
    {
        // where clause
        $this->db->where('id', $id);
        // execute query
        $query = $this->db->update('information', $payload);

        //check if query is success or fail
        if ($query) {
            return json_encode(array(
                'method' => 'PUT',
                'status' => 'success',
                'data' => $payload,
            ));
        } else {
            return json_encode(array(
                'method' => 'PUT',
                'status' => 'fail',
                'data' => [],
                'message' => 'Failed to Update'
            ));
        }
    }

    /**
     * HTTP DELETE Request
     *
     * @param $id
     * @param $payload
     */
    public function httpDelete($id, $payload)
    {

        // Explode the ids 
        $selected_id = ['id' => is_string($id) ? explode(",", $id) : null];


        if (!is_null($selected_id['id'])) {
            if (count($selected_id['id'])) {
                // If there are, use the IN operator to search for those specific ids in the 'id' column
                $this->db->where('id', $payload['id'], 'IN');
            }
        } else {
            // If there are no selected ids, use the normal operator to search for the single id in the 'id' column
            $this->db->where('id', $payload['id']);
        }

        // Execute query
        $query = $this->db->delete('information');

        if ($this->db->count == 0) {
            // id not found, return error response
            return json_encode(array(
                'method' => 'DELETE',
                'status' => 'fail',
                'data' => [],
                'message' => 'ID not found'
            ));
        }

        // check if success or fail
        if ($query) {
            return json_encode(array(
                'method' => 'DELETE',
                'status' => 'success',
                'data' => $payload,
            ));
            return;
        } else {
            return json_encode(array(
                'method' => 'DELETE',
                'status' => 'fail',
                'data' => [],
                'message' => 'Failed to Delete'
            ));
        }
    }
}

// Identifier if what type of request
// $request_method = $_SERVER['REQUEST_METHOD'];

// // For GET,POST,PUT & DELETE Request
// if ($request_method === 'GET') {
//     $received_data = $_GET;
// } else {
//     //check if method is PUT or DELETE, and get the ids on URL
//     if ($request_method === 'PUT' || $request_method === 'DELETE') {
//         $request_uri = $_SERVER['REQUEST_URI'];

//         $ids = null;
//         $exploded_request_uri = array_values(explode("/", $request_uri));


//         $last_index = count($exploded_request_uri) - 1;


//         $ids = $exploded_request_uri[$last_index];
//     }
// }


// //payload data
// $received_data = json_decode(file_get_contents('php://input'), true);



// $api = new API;


// //Checking if what type of request and designating to specific functions
// switch ($request_method) {
//     case 'GET':
//         $api->httpGet($received_data);
//         break;
//     case 'POST':
//         $api->httpPost($received_data);
//         break;
//     case 'PUT':
//         $api->httpPut($ids, $received_data);
//         break;
//     case 'DELETE':
//         $api->httpDelete($ids, $received_data);
//         break;
// }
