<?php

require_once 'API.php';

use PHPUnit\Framework\TestCase;

class APIFailTest extends TestCase
{
    protected function setUp(): void
    {
        $this->api = new API();
    }

    // Failed tests

    public function testFailedHttpPost()
    {
        $payload = array(
            'id' => '23',
            'first_name' => 'failed test',
            'middle_name' => 'test',
            'last_name' => 'last test',
            'contact_number' => '12345',
        );

        $result = json_decode($this->api->httpPost($payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'fail');
        $this->assertArrayHasKey('data', $result);
    }

    public function testFailedHttpGet()
    {
        $payload = array(
            'id' => '9999'
        );

        $result = json_decode($this->api->httpGet($payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'fail');
        $this->assertArrayHasKey('data', $result);
    }

    public function testFailedHttpPut()
    {
        $payload = array(
            'id' => '9999',
            'first_name' => 'Test',
            'middle_name' => 'test',
            'last_name' => 'last test',
            'contact_number' => 654655
        );

        $result = json_decode($this->api->httpGet($payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'fail');
        $this->assertArrayHasKey('data', $result);
    }

    public function testFailedHttpDelete()
    {
        $payload = array(
            'id' => '9999',
        );

        $result = json_decode($this->api->httpGet($payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'fail');
        $this->assertArrayHasKey('data', $result);
    }
}
