<?php

require_once 'API.php';

use PHPUnit\Framework\TestCase;

class APITest extends TestCase
{
    protected function setUp(): void
    {
        $this->api = new API();
    }

    public function testPayload()
    {
        $payload = array(
            'first_name' => 'Test',
            'middle_name' => 'test',
            'last_name' => 'last test',
            'contact_number' => 654655
        );

        $this->assertIsArray($payload);
        $this->assertNotEmpty($payload);

        return $payload;
    }

    /**
     * @depends testPayload
     */

    public function testHttpPost($payload)
    {
        $_SERVER['REQUEST_METHOD'] = 'POST';

        $result = json_decode($this->api->httpPost($payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);

        return $result;
    }

    /**
     * @depends testHttpPost
     */

    public function testHttpGet($result)
    {
        $_SERVER['REQUEST_METHOD'] = 'GET';

        $id = $result['data']['id'];
        $payload = array(
            'id' => $id
        );

        $result = json_decode($this->api->httpGet($payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);

        return $result;
    }

    /**
     * @depends testPayload
     * @depends testHttpGet
     */

    public function testHttpPut($payload, $result)
    {
        $_SERVER['REQUEST_METHOD'] = 'PUT';

        $id = $result['data']['id'];
        $payload = array_merge(array('id' => $id), $payload);

        $result = json_decode($this->api->httpPut($id, $payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);

        return $result;
    }

    /**
     * @depends testHttpGet
     */

    public function testHttpDelete($result)
    {
        $_SERVER['REQUEST_METHOD'] = 'DELETE';

        $id = $result['data']['id'];

        $payload = array(
            'id' => $id
        );

        $result = json_decode($this->api->httpDelete($id, $payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
    }
}
